package algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class B_SubGraph extends SubGraph {
	
	private Node _n1;
	private Node _n2;
	private Node _n3;
	private Node _n4;
	private Node _n5;
	
	/**
	 * Les couleurs des nodes du sous-graphe B sont initialisées avec à partir de 20
	 */
	public B_SubGraph() {
		super();
		_nbOfNodes = 5;

		_n1 = new Node("B", 1, 21);
		_n2 = new Node("B", 2, 22);
		_n3 = new Node("B", 3, 23);
		_n4 = new Node("B", 4, 24);
		_n5 = new Node("B", 5, 25);
		
		_localNodes = new HashMap<Integer, Node>();
		_localNodes.put(1, _n1);
		_localNodes.put(2, _n2);
		_localNodes.put(3, _n3);
		_localNodes.put(4, _n4);
		_localNodes.put(5, _n5);
		
		_innerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links1i = new ArrayList<Node>();
		ArrayList<Node> links2i = new ArrayList<Node>();
		ArrayList<Node> links3i = new ArrayList<Node>();
		ArrayList<Node> links4i = new ArrayList<Node>();
		ArrayList<Node> links5i = new ArrayList<Node>();
		links1i.add(_n2);
		links2i.add(_n1);
		links2i.add(_n3);
		links2i.add(_n5);
		links3i.add(_n2);
		links4i.add(_n5);
		links5i.add(_n2);
		links5i.add(_n4);
		_innerLinks.put(_n1, links1i);
		_innerLinks.put(_n2, links2i);
		_innerLinks.put(_n3, links3i);
		_innerLinks.put(_n4, links4i);
		_innerLinks.put(_n5, links5i);
		
		_outerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links1o = new ArrayList<Node>();
		ArrayList<Node> links3o = new ArrayList<Node>();
		ArrayList<Node> links4o = new ArrayList<Node>();
		ArrayList<Node> links5o = new ArrayList<Node>();
		links1o.add(new Node("A",1));
		links3o.add(new Node("A",1));
		links3o.add(new Node("C",2));
		links4o.add(new Node("A",5));
		links4o.add(new Node("A",7));
		links5o.add(new Node("C",2));
		_outerLinks.put(_n1, links1o);
		_outerLinks.put(_n3, links3o);
		_outerLinks.put(_n4, links4o);
		_outerLinks.put(_n5, links5o);

	}

}

package algo;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.util.leap.Iterator;

import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public abstract class SubGraph implements ISubGraph {
	
	int _nbOfNodes;
	Map<Integer,Node> _localNodes;
	Map<Node, List<Node>> _innerLinks;
	Map<Node, List<Node>> _outerLinks;
	SortedSet<Integer> _delta_p1;
	SortedSet<Integer> _delta_p1_w;
	SubGraph _a_subGraph;
	SubGraph _b_subGraph;
	SubGraph _c_subGraph;
	
	public SubGraph() {
		_delta_p1 = new TreeSet<Integer>();
		_delta_p1.add(0);
		_delta_p1.add(1);
		_delta_p1.add(2);
		_delta_p1.add(3);
		_delta_p1_w = new TreeSet<Integer>(_delta_p1);
	}
	
	/* (non-Javadoc)
	 * @see algo.ISubGraph#startOuterNodeColoring()
	 */
	@Override
	public int startOuterNodeColoring(Agent a){
		Node n = findNotColored_inOuterNode();
		int color = -1;
		if (n != null) {
			selectColorNode(n);
			for (Node remoteNode : _outerLinks.get(n)) {
				try {
					color = sendMessage(n, remoteNode, n.getColor(), a);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return color;
	}
	
	/* (non-Javadoc)
	 * @see algo.ISubGraph#printNodesColor()
	 */
	@Override
	public void printNodesColor() {
		for (Map.Entry<Integer, Node> entry : _localNodes.entrySet()) {
			System.out.println("  " + entry.getValue().toString() + " : " + Integer.toString(entry.getValue().getColor()));
		}
	}
	
	/* (non-Javadoc)
	 * @see algo.ISubGraph#fillInnerNodeColor()
	 */
	@Override
	public void fillInnerNodeColor() {
		for (Map.Entry<Integer, Node> entry : _localNodes.entrySet()) {
			if (entry.getValue().getColor() > 3) {
				SortedSet<Integer> neighborColors = getSetOfInnerNeighborColors(entry.getValue());
				_delta_p1_w.removeAll(neighborColors);
				entry.getValue().setColor(_delta_p1_w.first());
				_delta_p1_w = new TreeSet<Integer>(_delta_p1);				
			}
		}
	}
	
	protected Node findNotColored_inOuterNode() {
		for (Map.Entry<Node, List<Node>> entry : _outerLinks.entrySet()) {
			if (entry.getKey().getColor() > 3) {
				return entry.getKey();
			}
		}
		return null;
	}
	
	protected void selectColorNode(Node n) {
		SortedSet<Integer> usedColors = new TreeSet<Integer>();
		for (Node neighborNode : _innerLinks.get(n)) {
			if (neighborNode.getColor() < 4) {
				usedColors.add(neighborNode.getColor());
			}
		}
		_delta_p1_w.removeAll(usedColors);
		n.setColor(_delta_p1_w.first());
		_delta_p1_w = new TreeSet<Integer>(_delta_p1);
	}
	
	protected int sendMessage(Node origin, Node remoteNode, int color, Agent a) throws InterruptedException{
		
		int colorSetByOtherNode = -1;
		
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
        msg.setContent( "setOuterNodeColor#origin:"+origin.toString()+"#remote:"+remoteNode.toString()+"#color:"+color );
        msg.addReceiver( new AID( remoteNode.getGroup() , AID.ISLOCALNAME) );
        System.out.println("message: "+msg.toString());
        a.send(msg);
		
        ACLMessage  answer = a.blockingReceive();
        if(answer!=null){
	        System.out.println("answered "+answer.getContent());
	        
	        colorSetByOtherNode = (Integer.parseInt(answer.getContent()));
        }
		/*if (remoteNode.getGroup().equals("A")) {
			colorSetByOtherNode = _a_subGraph.setOuterNodeColor(origin, remoteNode, a);
		} else if (remoteNode.getGroup().equals("B")) {
			colorSetByOtherNode = _b_subGraph.setOuterNodeColor(origin, remoteNode, a);
		} else {
			colorSetByOtherNode = _c_subGraph.setOuterNodeColor(origin, remoteNode, a);
		}*/
		
		return colorSetByOtherNode;
	}
	
	// Méthode qui doit être remplacée par une communication entre agents...
	protected int setOuterNodeColor(Node origin, Node node, int remoteColor, Agent a) throws InterruptedException {
		int localColor = _localNodes.get(node.getUnit()).getColor();
		if (localColor < 4) {
			if (localColor == remoteColor) {
				SortedSet<Integer> remoteColors = getSetOfOuterNeighborColors(_localNodes.get(node.getUnit()), a);
				remoteColors.addAll(getSetOfInnerNeighborColors(_localNodes.get(node.getUnit())));
				remoteColors.add(remoteColor);
				_delta_p1_w.removeAll(remoteColors);
				localColor = _delta_p1_w.first();
				_delta_p1_w = new TreeSet<Integer>(_delta_p1);
			}
		} else {
			SortedSet<Integer> remoteColors = getSetOfOuterNeighborColors(_localNodes.get(node.getUnit()), a);
			remoteColors.addAll(getSetOfInnerNeighborColors(_localNodes.get(node.getUnit())));
			remoteColors.add(remoteColor);
			_delta_p1_w.removeAll(remoteColors);
			localColor = _delta_p1_w.first();
			_delta_p1_w = new TreeSet<Integer>(_delta_p1);
		}
		_localNodes.get(node.getUnit()).setColor(localColor);
		try {
			for (Node remoteNode : _outerLinks.get(_localNodes.get(node.getUnit()))) {
				if (!remoteNode.equals(origin)) {
					sendMessage(_localNodes.get(node.getUnit()), remoteNode, localColor, a);
				}
			}
		} catch (NullPointerException e) {
			// Node node does not have outer links
		}
		return localColor;
	}
	
	protected SortedSet<Integer> getSetOfOuterNeighborColors(Node localNode, Agent a) throws InterruptedException {
		SortedSet<Integer> neighborNodeColors = new TreeSet<Integer>();
		for (Node remoteNeighbor : _outerLinks.get(localNode)) {
			
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
            msg.setContent( "getNodeColor#"+remoteNeighbor.getUnit() );
            System.out.println("Sending: getNodeColor#"+remoteNeighbor.getUnit()+" "+remoteNeighbor.getGroup());
            msg.addReceiver( new AID( remoteNeighbor.getGroup() , AID.ISLOCALNAME) );
            System.out.println("message: "+msg.toString());
            a.send(msg);
			
            ACLMessage  answer = a.blockingReceive();
            if(answer!=null){
            System.out.println("answered "+answer.getContent());
            
            neighborNodeColors.add(Integer.parseInt(answer.getContent()));
            
            }
			
		}
		System.out.println(" fin getSetOfOuterNeighborColors ");
		return neighborNodeColors;
	}
	
	protected SortedSet<Integer> getSetOfInnerNeighborColors(Node localNode) {
		SortedSet<Integer> neighborNodeColors = new TreeSet<Integer>();
		for (Node localNeighbor : _innerLinks.get(localNode)) {
			neighborNodeColors.add(localNeighbor.getColor());
		}
		return neighborNodeColors;
	}
	
	// Méthode qui doit être remplacée par une communication entre agents...
	public int getNodeColor(int unit) {
		return _localNodes.get(unit).getColor();
	}
	
	/* (non-Javadoc)
	 * @see algo.ISubGraph#setSubGraphs(algo.SubGraph, algo.SubGraph, algo.SubGraph)
	 */
	@Override
	public void setSubGraphs(SubGraph a_subGraph, SubGraph b_subGraph, SubGraph c_subGraph) {
		_a_subGraph = a_subGraph;
		_b_subGraph = b_subGraph;
		_c_subGraph = c_subGraph;
	}

}


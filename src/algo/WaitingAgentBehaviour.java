package algo;

import java.io.ObjectInputStream;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class WaitingAgentBehaviour extends CyclicBehaviour{
	
	//private Agent ag;
	int s;
	ISubGraph subG;
	
    public WaitingAgentBehaviour(ISubGraph sub) { 
    	 super();  
         //this.ag = a; 
         this.subG = sub;
         s=0;
    }
              
    public void action() 
    {
    	if(s==0){
    		
    		System.out.println( " - " + myAgent.getLocalName() + " <- Listening");
	       ACLMessage msg= myAgent.receive();
	       //System.out.println( " - " + myAgent.getLocalName() + " <- Recieving"+ msg.getContent());
           
	       if (msg!=null){
	           System.out.println( " - " + myAgent.getLocalName() + " <- " + msg.getContent() );
	           
	           String st = msg.getContent();
	           int split = st.indexOf("#");
	           String commande = st.substring(0, split);
	           
	           if(commande.equals("getNodeColor")){
	        	   String noeud = st.substring(split+1);
	        	   System.out.println("commande:"+commande+" sur noeud "+noeud);
	        	   int color = subG.getNodeColor(Integer.parseInt(noeud));
	        	   
	        	   ACLMessage reply = msg.createReply();
		           reply.setPerformative(ACLMessage.INFORM);
		           reply.setContent(Integer.toString(color));
		           myAgent.send(reply);
	        	   
	           }else if(commande.equals("setOuterNodeColor")){
	        	   
	        	   System.out.println("setOuterNodeColor:");
	        	   int split2 = st.indexOf("#origin:");
	        	   int split3 = st.indexOf("#remote:");
	        	   int split4 = st.indexOf("#color:");
	        	   
		           String originNode = st.substring(split2+8, split3);
		           String remoteNode = st.substring(split3+8, split4);
		           String nodeColor = st.substring(split4+7);
		           Node origin;
		           //origin.fromString(originNode);// = (Node) fromString( originNode );
		           
		            
	        	   /*int color = subG.getNodeColor(Integer.parseInt(noeud));
	        	   
	        	   ACLMessage reply = msg.createReply();
		           reply.setPerformative(ACLMessage.INFORM);
		           reply.setContent(Integer.toString(color));
		           myAgent.send(reply);*/
	        	   
	           }
	           
	           /*ACLMessage reply = msg.createReply();
	           reply.setPerformative(ACLMessage.INFORM);
	           reply.setContent("5");
	           myAgent.send(reply);*/
	           //s=1;
	           
	       }
	       block();
    		    
    		
	       	/* ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
	       	answer.setContent( "test" );
	       	answer.addReceiver( new AID( "aq1" , AID.ISLOCALNAME) );
	            ag.send(msg);*/
	   	 
	   	 
    		
    	}
    	//System.out.println( "act");
   	
   	 //this.finished = true;
    }

}

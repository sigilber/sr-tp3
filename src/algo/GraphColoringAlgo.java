package algo;

public class GraphColoringAlgo {
	
	private static SubGraph a_subGraph;
	private static SubGraph b_subGraph;
	private static SubGraph c_subGraph;
	
	public static void main(String[] args) {
		a_subGraph = new A_SubGraph();
		b_subGraph = new B_SubGraph();
		c_subGraph = new C_SubGraph();
		a_subGraph.setSubGraphs(a_subGraph, b_subGraph, c_subGraph);
		b_subGraph.setSubGraphs(a_subGraph, b_subGraph, c_subGraph);
		c_subGraph.setSubGraphs(a_subGraph, b_subGraph, c_subGraph);

		int res;
		
		do {
			//res = a_subGraph.startOuterNodeColoring();
			res = -1;
		} while (res != -1);
		
		do {
			//res = b_subGraph.startOuterNodeColoring();
			res = -1;
		} while (res != -1);
		
		do {
			//res = c_subGraph.startOuterNodeColoring();
			res = -1;
		} while (res != -1);
		
		
		a_subGraph.fillInnerNodeColor();
		b_subGraph.fillInnerNodeColor();
		c_subGraph.fillInnerNodeColor();
		
		printNodeColors();
	}
	
	private static void printNodeColors() {
		System.out.println("A subgraph node's color : ");
		a_subGraph.printNodesColor();
		System.out.println("-------------------------------------------------------");
		System.out.println("B subgraph node's color : ");
		b_subGraph.printNodesColor();
		System.out.println("-------------------------------------------------------");
		System.out.println("C subgraph node's color : ");
		c_subGraph.printNodesColor();
	}

}

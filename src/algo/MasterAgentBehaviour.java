package algo;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;


public class MasterAgentBehaviour extends OneShotBehaviour {
	
	//private Agent ag;
	int s;
	ISubGraph subG;
	
    public MasterAgentBehaviour(ISubGraph sub) { 
    	 super();  
         //this.ag = a; 
         this.subG = sub;
         s=0;
    }
              
    public void action() 
    {
    	if(s==0){
    		int res;
    		
    		do {
    			res = subG.startOuterNodeColoring(myAgent);
    		} while (res != -1);
    		
    		/*
	    		 String[] a = {"Bleu", "Rouge", "Vert"};
	   	 for(int i=0; i<a.length ;i++){
	       	 ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
	            msg.setContent( a[i] );
	            msg.addReceiver( new AID( "aq1" , AID.ISLOCALNAME) );
	            ag.send(msg);
	   	 }*/
	   	 s=1;
    		
    	}
   	 //this.finished = true;
    }
    
    private boolean finished = false;
    //public  boolean done() {  return finished;  }

}

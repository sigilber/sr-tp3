package algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class A_SubGraph extends SubGraph {
	
	private Node _n1;
	private Node _n2;
	private Node _n3;
	private Node _n4;
	private Node _n5;
	private Node _n6;
	private Node _n7;
	
	/**
	 * Les couleurs des nodes du sous-graphe A sont initialisées avec à partir de 10
	 */
	public A_SubGraph() {
		super();
		_nbOfNodes = 7;
		
		_n1 = new Node("A", 1, 11);
		_n2 = new Node("A", 2, 12);
		_n3 = new Node("A", 3, 13);
		_n4 = new Node("A", 4, 14);
		_n5 = new Node("A", 5, 15);
		_n6 = new Node("A", 6, 16);
		_n7 = new Node("A", 7, 17);
		
		_localNodes = new HashMap<Integer, Node>();
		_localNodes.put(1, _n1);
		_localNodes.put(2, _n2);
		_localNodes.put(3, _n3);
		_localNodes.put(4, _n4);
		_localNodes.put(5, _n5);
		_localNodes.put(6, _n6);
		_localNodes.put(7, _n7);

		_innerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links1i = new ArrayList<Node>();
		ArrayList<Node> links2i = new ArrayList<Node>();
		ArrayList<Node> links3i = new ArrayList<Node>();
		ArrayList<Node> links4i = new ArrayList<Node>();
		ArrayList<Node> links5i = new ArrayList<Node>();
		ArrayList<Node> links6i = new ArrayList<Node>();
		ArrayList<Node> links7i = new ArrayList<Node>();
		links1i.add(_n2);
		links2i.add(_n1);
		links2i.add(_n3);
		links3i.add(_n2);
		links3i.add(_n4);
		links3i.add(_n7);
		links4i.add(_n3);
		links4i.add(_n5);
		links5i.add(_n4);
		links5i.add(_n6);
		links6i.add(_n5);
		links6i.add(_n7);
		links7i.add(_n6);
		links7i.add(_n3);
		_innerLinks.put(_n1, links1i);
		_innerLinks.put(_n2, links2i);
		_innerLinks.put(_n3, links3i);
		_innerLinks.put(_n4, links4i);
		_innerLinks.put(_n5, links5i);
		_innerLinks.put(_n6, links6i);
		_innerLinks.put(_n7, links7i);
		
		_outerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links1o = new ArrayList<Node>();
		ArrayList<Node> links4o = new ArrayList<Node>();
		ArrayList<Node> links5o = new ArrayList<Node>();
		ArrayList<Node> links7o = new ArrayList<Node>();
		links1o.add(new Node("B",1));
		links1o.add(new Node("B",3));
		links4o.add(new Node("C",5));
		links5o.add(new Node("C",4));
		links5o.add(new Node("B",4));
		links7o.add(new Node("B",4));
		_outerLinks.put(_n1, links1o);
		_outerLinks.put(_n4, links4o);
		_outerLinks.put(_n5, links5o);
		_outerLinks.put(_n7, links7o);
	}

}

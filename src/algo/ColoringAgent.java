package algo;

import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.ThreadedBehaviourFactory;
import jade.lang.acl.ACLMessage;

public class ColoringAgent extends Agent {
	
	private ISubGraph subG;
	ThreadedBehaviourFactory tbf;
	
	 protected void setup() 
     {
		 Object[] args = getArguments();
		 tbf=new ThreadedBehaviourFactory();
		 String s = (String) args[0];
		 //System.out.println("Creating Agent, P="+s);
		 
		 if(s.equals("A")){
			 System.out.println("Creating A Agent");
			 subG = new A_SubGraph(); 
			 WaitingAgentBehaviour b = new WaitingAgentBehaviour(subG);
			 addBehaviour(tbf.wrap(b));
			 addBehaviour(new MasterAgentBehaviour(subG));
		 }
		 else if(s.equals("B")){
			 System.out.println("Creating B Agent");
			 
			 subG = new B_SubGraph();
			 WaitingAgentBehaviour b = new WaitingAgentBehaviour(subG);
			 addBehaviour(tbf.wrap(b));
		 }
		 else{
			 System.out.println("Creating C Agent");
			 
			 subG = new C_SubGraph();
			 WaitingAgentBehaviour b = new WaitingAgentBehaviour(subG);
			 addBehaviour(tbf.wrap(b));
		 }
			 
	 }
     
 }



package algo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class C_SubGraph extends SubGraph {
	
	private Node _n1;
	private Node _n2;
	private Node _n3;
	private Node _n4;
	private Node _n5;
	private Node _n6;
	
	/**
	 * Les couleurs des nodes du sous-graphe C sont initialisées avec à partir de 30
	 */
	public C_SubGraph() {
		super();
		_nbOfNodes = 6;
		
		_n1 = new Node("C", 1, 31);
		_n2 = new Node("C", 2, 32);
		_n3 = new Node("C", 3, 33);
		_n4 = new Node("C", 4, 34);
		_n5 = new Node("C", 5, 35);
		_n6 = new Node("C", 6, 36);
		
		_localNodes = new HashMap<Integer, Node>();
		_localNodes.put(1, _n1);
		_localNodes.put(2, _n2);
		_localNodes.put(3, _n3);
		_localNodes.put(4, _n4);
		_localNodes.put(5, _n5);
		_localNodes.put(6, _n6);

		_innerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links1i = new ArrayList<Node>();
		ArrayList<Node> links2i = new ArrayList<Node>();
		ArrayList<Node> links3i = new ArrayList<Node>();
		ArrayList<Node> links4i = new ArrayList<Node>();
		ArrayList<Node> links5i = new ArrayList<Node>();
		ArrayList<Node> links6i = new ArrayList<Node>();
		links1i.add(_n2);
		links2i.add(_n1);
		links2i.add(_n3);
		links3i.add(_n2);
		links3i.add(_n4);
		links3i.add(_n5);
		links4i.add(_n3);
		links4i.add(_n6);
		links5i.add(_n3);
		links5i.add(_n6);
		links6i.add(_n4);
		links6i.add(_n5);
		_innerLinks.put(_n1, links1i);
		_innerLinks.put(_n2, links2i);
		_innerLinks.put(_n3, links3i);
		_innerLinks.put(_n4, links4i);
		_innerLinks.put(_n5, links5i);
		_innerLinks.put(_n6, links6i);
		
		_outerLinks = new HashMap<Node, List<Node>>();
		ArrayList<Node> links2o = new ArrayList<Node>();
		ArrayList<Node> links4o = new ArrayList<Node>();
		ArrayList<Node> links5o = new ArrayList<Node>();
		links2o.add(new Node("B",3));
		links2o.add(new Node("B",5));
		links4o.add(new Node("A",5));
		links5o.add(new Node("A",4));
		_outerLinks.put(_n2, links2o);
		_outerLinks.put(_n4, links4o);
		_outerLinks.put(_n5, links5o);
	}

}

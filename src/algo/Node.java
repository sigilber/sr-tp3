package algo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;

public class Node implements Serializable{
	
	private String _group;
	private int _unit;
	private int _color;
	
	public Node(String group, int unit, int color) {
		_unit = unit;
		_color = color;
		_group = group;
	}
	
	public Node(String group, int unit) {
		this(group, unit, 99999999);
	}
		
	public String toString() {
		return _group + Integer.toString(_unit);
	}
		
	public String getGroup() {
		return _group;
	}
		
	public int getUnit() {
		return _unit;
	}
		
	public boolean equals(Node n) {
		return (n.getUnit() == _unit) && (n.getGroup().equals(_group));
	}

	public int getColor() {
		return _color;
	}

	public void setColor(int color) {
		_color = color;
	}
	
}

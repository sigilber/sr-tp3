package algo;

import jade.core.Agent;

public interface ISubGraph {

	//public abstract int startOuterNodeColoring();

	public abstract void printNodesColor();

	public abstract void fillInnerNodeColor();

	public abstract void setSubGraphs(SubGraph a_subGraph, SubGraph b_subGraph,
			SubGraph c_subGraph);

	public int startOuterNodeColoring(Agent a);
	
	public int getNodeColor(int node);

}